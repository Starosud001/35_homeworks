"use strict";
/*
Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
При вызове функция должна спросить у вызывающего имя и фамилию.
Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, 
соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
 Вывести в консоль результат выполнения функции.
*/

function createNewUser() {
    const firstName = prompt("Enter your first name");
    const lastName = prompt("Enter your last name");

    return {
        firstName,
        lastName,
        getLogin() {
            // closure solution
            // return firstName.charAt(0).concat(lastName).toLowerCase();

            return this.firstName.charAt(0).concat(this.lastName).toLowerCase();
        },
    };
}
const user = createNewUser();
console.log(user, user.getLogin());
