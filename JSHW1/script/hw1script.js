"use strict";

/*
let - область видимости блочная, 
var - область димости глабальная,
*/


let name;
let age;
let answer;

do {
    name = prompt("Enter your name");
} while (!name || name == "");

do {
    age = prompt("Enter your age");
} while (!age || isNaN(+age));

if (age < 18) {
        alert("You are not allowed to visit this website");
} else if (age <= 22) {
    answer = confirm("Are you sure you want to continue?");
    if (answer) {
        alert(`Welcome ${name}`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
        alert(`Welcome ${name}`);
}
