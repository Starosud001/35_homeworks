"use strict";

/*
Задание
Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем. 

Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните 
ее следующим функционалом:

При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.

Создать метод getAge() который будет возвращать сколько пользователю лет.

Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную 
с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
*/

function createNewUser() {
    const firstName = prompt("Enter your first name");
    const lastName = prompt("Enter your last name");
    const birthday = prompt("Enter your birth date");

    const getFullYear = (date = new Date()) => date.getFullYear();

    const parseDate = date => {
        const [day, month, year] = date.split('.');

        return new Date(`${month}/${day}/${year}`);
    }

    return {
        firstName,
        lastName,
        birthday,
        getLogin() {
            // closure solution
            // return firstName.charAt(0).concat(lastName).toLowerCase();

            return this.firstName.charAt(0).concat(this.lastName).toLowerCase();
        },
        getAge() {
            return getFullYear() - getFullYear(parseDate(this.birthday));
        },
        getPassword() {
            return this.firstName.charAt(0).toUpperCase().concat(this.lastName.toLowerCase()).concat(getFullYear(parseDate(birthday)));
        },
    };
}
const user = createNewUser();
console.log(user, user.getLogin(), user.getAge(), user.getPassword());