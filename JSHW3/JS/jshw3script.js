"use strict";
/*
Теоретический вопрос

Описать своими словами для чего вообще нужны функции в программировании.
Описать своими словами, зачем в функцию передавать аргумент.


Задание
Реализовать функцию, которая будет производить математические операции с введеными пользователем числами. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).

Технические требования:

Считать с помощью модального окна браузера два числа.
Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
Создать функцию, в которую передать два значения и операцию.
Вывести в консоль результат выполнения функции.


Необязательное задание продвинутой сложности:

После ввода данных добавить проверку их корректности. Если пользователь не ввел числа, либо при вводе указал не числа, - спросить оба числа заново (при этом значением по умолчанию для каждой из переменных должна быть введенная ранее информация).

*/

/*
let num1
let num2
let operation


do{
    num1 =+prompt("enter number1")
}
while(!num1 || isNaN(num1))

do{
    num2 =+prompt("enter number2")
}
while(!num2 || isNaN(num2))

do{
    operation = prompt("select operation") + ''
}
while(!operation)



let result = calcNums(num1, num2,operation)

console.log(result)

function calcNums(num1, num2, operation) {

    let res
    console.log(operation)
    switch(operation){
        case operation = ('+'): 
            res = num1 + num2;
            break
        case operation == "-" : 
            res = num1 - num2;
            break
        case operation == "*" : 
            res = num1 * num2;
            break
        case operation == "/" : 
            res = num1 / num2;
            break
        
    }

    return res
}
*/

function calcNums(num1, num2, operation) {
  switch (operation) {
    case "+":
      return num1 + num2;
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      return num1 / num2;
  }
}

function promtValue(valueName) {
  const value = prompt()
}

function main() {
  let num1;
  let num2;
  let operation;
  // todo: move each do..while into separate function
  do {
    num1 = +prompt("enter number1");
    console.log(num1)
  } while (!num1 || isNaN(num1));
  do {
    num2 = +prompt("enter number2");
    console.log(num2)
  } while (!num2 || isNaN(num2));
  do {
    operation = prompt("select operation") + "";
    console.log(operation)
  } while (!operation);

  const result = calcNums(num1, num2, operation);

  console.log(result);
}

main();


