"use strict";



const main = () => {
    let counter = 0;
        
    const images = [...document.getElementsByClassName('image-to-show')];
    const start = () => setInterval(() => {
        images[counter].classList.add('hidden');

        counter = counter === 3 ? 0 : counter + 1;
        
        images[counter].classList.remove('hidden');
    }, 3000);

    let timerId = start();

    const pauseBtn = document.getElementById('pause');
    const resumeBtn = document.getElementById('resume');

    pauseBtn.addEventListener('click', () => {
        clearInterval(timerId);
        resumeBtn.classList.remove('hidden');
        pauseBtn.classList.add('hidden');
    });


    resumeBtn.addEventListener('click', () => {
        timerId = start();
        pauseBtn.classList.remove('hidden');
        resumeBtn.classList.add('hidden');
    });

}

main();
