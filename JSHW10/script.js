'use strict';

const toggle = event => {
    const icon = event.target;
    const wrapper = icon.parentNode;
    const input = wrapper.querySelector('input');

    icon.classList.add('hidden');

    if (icon.classList.contains('fa-eye')) {
        wrapper.getElementsByClassName('fa-eye-slash')[0].classList.remove('hidden');
        input.type = 'text';
    } else {
        wrapper.getElementsByClassName('fa-eye')[0].classList.remove('hidden');
        input.type = 'password';
    }
};

const compare = event => {
    event.preventDefault();
    const form = event.target.parentNode;
    const [password, confirmationPassword] = [...form.getElementsByClassName('password-input')];

    if (password.value === confirmationPassword.value) {
        alert('You are welcome');
    } else {
        alert('Нужно ввести одинаковые значения');
    }
};

// TODO: validate each input field for emptiness
// TODO: refactor 'DRY' - don't repeat yourself 
const main = () => {
    const icons = [...document.getElementsByClassName('fa-eye'), ...document.getElementsByClassName('fa-eye-slash')];
    const submitBtn = document.getElementById('submit');

    icons.forEach(icon => icon.addEventListener('click', toggle));

    submitBtn.addEventListener('click', compare);
};

main();