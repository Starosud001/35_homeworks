"use strict";

const toKeyCode = key => (key !== 'Enter') ? `Key${key}` : key;

const main = () => {
    const buttons = document.getElementsByClassName("btn");

    const handleKeydown = event => {
        const key = event.code;
        
        for (const button of buttons) {
            button.classList.remove('btn-blue');

            if (key === toKeyCode(button.innerText)) {
                button.classList.add('btn-blue');
            }
        }
    }

    document.addEventListener('keydown', handleKeydown);
}

main();
