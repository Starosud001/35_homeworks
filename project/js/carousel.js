"use strict"

const makeEmployee = () => {
    const employeeMap = {
        "0": {
            id: 0,
            name: "Lisa",
            position : "UX Designer",
            description: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar o.",
            img : "./employeephoto/Layer6.png"
        },
        "1": {
            id: 1,
            name: "Misha",
            position : "DevOps",
            description: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis .",
            img : "./employeephoto/Layer7.png"
        },
        "2": {
            id: 2,
            name: "Hasan",
            position : "HR",
            description: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non .",
            img : "./employeephoto/Layer5.png"
        },
        "3": {
            id: 3,
            name: "Anna",
            position : "Accountant",
            description: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam du.",
            img : "./employeephoto/Layer8.png"
        }
    }

    const FIRST_EMPLOYEE = 0;
    const LAST_EMPLOYEE = 3;

    let current = 0;

    return ({
        next: () => {
            current = current === LAST_EMPLOYEE ? FIRST_EMPLOYEE : current + 1;

            return employeeMap[current]
        },
        prev: () => {
            current = current === FIRST_EMPLOYEE ? LAST_EMPLOYEE : current - 1;

            return employeeMap[current]
        },
        getById: id => {
            current = id;

            return employeeMap[id];
        },
        render: ({ id, name, position, description, img }) => {
            const employeeIcons = document.getElementsByClassName('employee-small-icon');
            for (const employeeIcon of employeeIcons) {
                const employeeId = +employeeIcon.dataset.employeeId;
                employeeIcon.className = employeeIcon.className.replace(' active', '');

                if (employeeId === id) {
                    employeeIcon.className = employeeIcon.className + ' active';
                    document.getElementById('employee-name').innerText = name; 
                    document.getElementById('employee-description').innerText = description;   
                    document.getElementById('employee-position').innerText = position; 
                    document.getElementById('employee-img').src = img;
                }
            }
        }
    })
}

const Employee = makeEmployee();

const nextButton = document.getElementById('move-next');
const prevButton = document.getElementById('move-prev');
const employeeIcons = document.getElementsByClassName('employee-small-icon');
// TODO: add render as a method available in employee object. 
nextButton.addEventListener('click', () => {
    const employee = Employee.next();

    Employee.render(employee);
});

prevButton.addEventListener('click', () => {
    const employee = Employee.prev();

    Employee.render(employee);
});

for (const employeeIcon of employeeIcons) {
    employeeIcon.addEventListener('click', event => {
        const employeeId = +event.currentTarget.dataset.employeeId;
        const employee = Employee.getById(employeeId);

        Employee.render(employee);
    })
}