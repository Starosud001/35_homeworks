'use strict';

(() => {
    const getByClassName = className => document.getElementsByClassName(className);
    const getNavTabs = () => getByClassName('nav-tab');
    const getServices = () => getByClassName('our-service-Web-Design');
    
    const handleNavTabClick = (event) => { 
        const activeServiceName = event.target.innerText;
    
        if (event.currentTarget.className.includes('active')) {
            return;
        }
    
        const serviceBlocks = getServices();
        for (const serviceBlock of serviceBlocks) {
            serviceBlock.style.display = 'none';
        } 
    
        const navTabs = getNavTabs();
        for (const navTab of navTabs) {
            navTab.className = navTab.className.replace(' active', '')
        }
    
        event.currentTarget.className += ' active';
        document.getElementById(activeServiceName).style.display = 'flex';
        
    }
    const navTabs = getNavTabs();
    
    for (const navTab of navTabs) {
        navTab.addEventListener('click', handleNavTabClick)
    }
    
})()



