"use strict";

function filterBy (arr,type) {
    let res = [];
    
    for(let elem of arr){
        if(typeof(elem) !== type){
            res.push(elem);
            
        }
    }
    return res;
}


let fil = filterBy(['hello', 'world', 23, '23', null], "string")
console.log(fil);